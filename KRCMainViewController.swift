//
//  KRCMainViewController.swift
//  KRCFramework
//
//  Created by Anil Sharma on 3/12/19.
//  Copyright © 2019 Anil Sharma. All rights reserved.
//

import UIKit
import WebKit

public class KRCMainViewController: UIViewController {
    
    
   var webView: WKWebView!
    

    override public func loadView() {
        let contentController = WKUserContentController()
        // Can be used to pass initial info like rc config or deep linking url
        let scriptSource = "window.urlToBeLoaded = `/#/eventPage`"
        let script = WKUserScript(source: scriptSource, injectionTime: .atDocumentStart, forMainFrameOnly: true)
        contentController.addUserScript(script)
//
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
//
        webView = WKWebView(frame: .zero, configuration: config)
        webView = WKWebView()
        view = webView
    }

    private func loadStaticPage() {
        if let htmlPath = Bundle(for: KRCMainViewController.self).path(forResource: "web/index", ofType: "html") {
                let url = URL(fileURLWithPath: htmlPath)
                let request = URLRequest(url: url)
                webView.load(request)
        } else {
            // show error page
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        loadStaticPage()
    }


}


