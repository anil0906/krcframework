//
//  KRCFramework.h
//  KRCFramework
//
//  Created by Anil Sharma on 3/12/19.
//  Copyright © 2019 Anil Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for KRCFramework.
FOUNDATION_EXPORT double KRCFrameworkVersionNumber;


//! Project version string for KRCFramework.
FOUNDATION_EXPORT const unsigned char KRCFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KRCFramework/PublicHeader.h>


