(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		0: 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([1,1,3]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("7ab88d2cd0178bc2bbbb");
__webpack_require__("7f76feceb22b8d28184e");
__webpack_require__("5015ee62a4c79dd067c3");
module.exports = __webpack_require__("c666a2f24dc3a8111cb2");


/***/ }),

/***/ "7ab88d2cd0178bc2bbbb":
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__.p=window.__CDN_ASSETS__;

/***/ }),

/***/ "7f76feceb22b8d28184e":
/***/ (function(module, exports) {

if(!Array.prototype.flatMap){Array.prototype.flatMap=function(fn){var _this=this;return this.reduce(function(acc,x,index){return acc.concat(fn(x,index,_this));},[]);};}

/***/ }),

/***/ "94e4968e996275b7b27a":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "4fe50ffc7983d80569ba",
	"./af.js": "4fe50ffc7983d80569ba",
	"./ar": "3f7f6568c894fbb46c29",
	"./ar-dz": "0a4b0969f58fca504d02",
	"./ar-dz.js": "0a4b0969f58fca504d02",
	"./ar-kw": "9dc1adf1367a833c5dde",
	"./ar-kw.js": "9dc1adf1367a833c5dde",
	"./ar-ly": "cccb904b2f064b592b02",
	"./ar-ly.js": "cccb904b2f064b592b02",
	"./ar-ma": "d81c92a0aa9e6c084db4",
	"./ar-ma.js": "d81c92a0aa9e6c084db4",
	"./ar-sa": "dee24f02b8ecd3a36176",
	"./ar-sa.js": "dee24f02b8ecd3a36176",
	"./ar-tn": "df08077061459e88529b",
	"./ar-tn.js": "df08077061459e88529b",
	"./ar.js": "3f7f6568c894fbb46c29",
	"./az": "9a25880d1491856f4e1e",
	"./az.js": "9a25880d1491856f4e1e",
	"./be": "ffa801cad665eebbfdc8",
	"./be.js": "ffa801cad665eebbfdc8",
	"./bg": "c81c2298ba3a83bd3b5b",
	"./bg.js": "c81c2298ba3a83bd3b5b",
	"./bn": "fa658b93b3c6b01c668e",
	"./bn.js": "fa658b93b3c6b01c668e",
	"./bo": "b30d6122d9094906930a",
	"./bo.js": "b30d6122d9094906930a",
	"./br": "ee0a94b9013e4e7ae788",
	"./br.js": "ee0a94b9013e4e7ae788",
	"./bs": "89a9e5355865ee89e959",
	"./bs.js": "89a9e5355865ee89e959",
	"./ca": "0a16bdb66e0525c325dc",
	"./ca.js": "0a16bdb66e0525c325dc",
	"./cs": "b7a0f42b9110b4ba030f",
	"./cs.js": "b7a0f42b9110b4ba030f",
	"./cv": "2b90ff6f0e337e46d5d6",
	"./cv.js": "2b90ff6f0e337e46d5d6",
	"./cy": "0d0ea3473f47b0d7b396",
	"./cy.js": "0d0ea3473f47b0d7b396",
	"./da": "7a8735ec68591dea5f8e",
	"./da.js": "7a8735ec68591dea5f8e",
	"./de": "5f6fa986f8050423684e",
	"./de-at": "7f7e87a36ad679cf294c",
	"./de-at.js": "7f7e87a36ad679cf294c",
	"./de-ch": "f949125f8f7b0502a634",
	"./de-ch.js": "f949125f8f7b0502a634",
	"./de.js": "5f6fa986f8050423684e",
	"./dv": "3073d71361f604d2822a",
	"./dv.js": "3073d71361f604d2822a",
	"./el": "4130747efcded86a1698",
	"./el.js": "4130747efcded86a1698",
	"./en-au": "517cd2b76c171df54c77",
	"./en-au.js": "517cd2b76c171df54c77",
	"./en-ca": "eb450040fb739424b607",
	"./en-ca.js": "eb450040fb739424b607",
	"./en-gb": "594dc41e90bb0b4ce7b3",
	"./en-gb.js": "594dc41e90bb0b4ce7b3",
	"./en-ie": "7f1a2a614d100ed33e07",
	"./en-ie.js": "7f1a2a614d100ed33e07",
	"./en-nz": "a04dc64dfdfc9ac4210a",
	"./en-nz.js": "a04dc64dfdfc9ac4210a",
	"./eo": "d8aa3f4072118ba0c74b",
	"./eo.js": "d8aa3f4072118ba0c74b",
	"./es": "8dca363c55e082fe5270",
	"./es-do": "24f7ae66e6bdc8652fba",
	"./es-do.js": "24f7ae66e6bdc8652fba",
	"./es.js": "8dca363c55e082fe5270",
	"./et": "0207368f4fca00662d61",
	"./et.js": "0207368f4fca00662d61",
	"./eu": "4d24b542827f0d35c2d4",
	"./eu.js": "4d24b542827f0d35c2d4",
	"./fa": "9cf1585e02829dff5f9a",
	"./fa.js": "9cf1585e02829dff5f9a",
	"./fi": "5b8ee8fca9bd9b066e05",
	"./fi.js": "5b8ee8fca9bd9b066e05",
	"./fo": "cef1ea44f0d3f6eb36e0",
	"./fo.js": "cef1ea44f0d3f6eb36e0",
	"./fr": "74e23c69ccb5a5eec403",
	"./fr-ca": "f79130304d5016e13ffb",
	"./fr-ca.js": "f79130304d5016e13ffb",
	"./fr-ch": "f08607bde9b355fddbe5",
	"./fr-ch.js": "f08607bde9b355fddbe5",
	"./fr.js": "74e23c69ccb5a5eec403",
	"./fy": "0d86e36306113c72bd77",
	"./fy.js": "0d86e36306113c72bd77",
	"./gd": "d9d35cbb598c476f7045",
	"./gd.js": "d9d35cbb598c476f7045",
	"./gl": "29e2d221e75f936e25f1",
	"./gl.js": "29e2d221e75f936e25f1",
	"./gom-latn": "d91ea0088c3c0ebdc87f",
	"./gom-latn.js": "d91ea0088c3c0ebdc87f",
	"./he": "624c722243ab1968c264",
	"./he.js": "624c722243ab1968c264",
	"./hi": "061d025a8797acc49a77",
	"./hi.js": "061d025a8797acc49a77",
	"./hr": "6a0c124ead216c897c0f",
	"./hr.js": "6a0c124ead216c897c0f",
	"./hu": "dce09320209f9a044b94",
	"./hu.js": "dce09320209f9a044b94",
	"./hy-am": "39a71a00fd6cfabeef30",
	"./hy-am.js": "39a71a00fd6cfabeef30",
	"./id": "a8eb5357a53ddba4d83c",
	"./id.js": "a8eb5357a53ddba4d83c",
	"./is": "0ccb71e8f62bf567199f",
	"./is.js": "0ccb71e8f62bf567199f",
	"./it": "35f77a330b2b59f483b2",
	"./it.js": "35f77a330b2b59f483b2",
	"./ja": "239d00735eb311c1c046",
	"./ja.js": "239d00735eb311c1c046",
	"./jv": "92711492583b83951b6d",
	"./jv.js": "92711492583b83951b6d",
	"./ka": "d31914898082af20b314",
	"./ka.js": "d31914898082af20b314",
	"./kk": "83d94a8a59b81194ec02",
	"./kk.js": "83d94a8a59b81194ec02",
	"./km": "331957b72f4beeaf2d4a",
	"./km.js": "331957b72f4beeaf2d4a",
	"./kn": "a0c1a30a96f4369ead33",
	"./kn.js": "a0c1a30a96f4369ead33",
	"./ko": "6de7f88f5c7d1ec2d27f",
	"./ko.js": "6de7f88f5c7d1ec2d27f",
	"./ky": "0747c8ca89dcf60580a2",
	"./ky.js": "0747c8ca89dcf60580a2",
	"./lb": "62019114b63dbd69721a",
	"./lb.js": "62019114b63dbd69721a",
	"./lo": "c2110f526b1ce19817cc",
	"./lo.js": "c2110f526b1ce19817cc",
	"./lt": "6df32c33af3d2e5fc11c",
	"./lt.js": "6df32c33af3d2e5fc11c",
	"./lv": "55b3e93988104c60a071",
	"./lv.js": "55b3e93988104c60a071",
	"./me": "e66100575f1ac6d00d4a",
	"./me.js": "e66100575f1ac6d00d4a",
	"./mi": "fd0033b452a90713b091",
	"./mi.js": "fd0033b452a90713b091",
	"./mk": "1ffc46d9756a7dd1c823",
	"./mk.js": "1ffc46d9756a7dd1c823",
	"./ml": "18c9a4d7cd02df34341e",
	"./ml.js": "18c9a4d7cd02df34341e",
	"./mr": "48ce3e4e5fdba4499ebb",
	"./mr.js": "48ce3e4e5fdba4499ebb",
	"./ms": "d892199478d896774453",
	"./ms-my": "de1a2ccd001fe36ceecc",
	"./ms-my.js": "de1a2ccd001fe36ceecc",
	"./ms.js": "d892199478d896774453",
	"./my": "b403e02a2233207d0c4d",
	"./my.js": "b403e02a2233207d0c4d",
	"./nb": "cb288475654d715d144b",
	"./nb.js": "cb288475654d715d144b",
	"./ne": "89e8ae3a87cc919f89a4",
	"./ne.js": "89e8ae3a87cc919f89a4",
	"./nl": "301790a531e8a33e9796",
	"./nl-be": "3daa408e1685eb5e8fbd",
	"./nl-be.js": "3daa408e1685eb5e8fbd",
	"./nl.js": "301790a531e8a33e9796",
	"./nn": "643270e1e47cf81f688e",
	"./nn.js": "643270e1e47cf81f688e",
	"./pa-in": "5c40bc9eec89d0d6802d",
	"./pa-in.js": "5c40bc9eec89d0d6802d",
	"./pl": "a4a8376f7fdb2d708907",
	"./pl.js": "a4a8376f7fdb2d708907",
	"./pt": "6177d8a6abfd93127496",
	"./pt-br": "e20444730613ccb91ab4",
	"./pt-br.js": "e20444730613ccb91ab4",
	"./pt.js": "6177d8a6abfd93127496",
	"./ro": "b2de2f54a02115863ee5",
	"./ro.js": "b2de2f54a02115863ee5",
	"./ru": "a884f82f1fcf323b0ed1",
	"./ru.js": "a884f82f1fcf323b0ed1",
	"./sd": "60abb66915d33f1d36b4",
	"./sd.js": "60abb66915d33f1d36b4",
	"./se": "40bfee84281d9b77f113",
	"./se.js": "40bfee84281d9b77f113",
	"./si": "a975fe4573e5d9e39086",
	"./si.js": "a975fe4573e5d9e39086",
	"./sk": "e0600ff36ac1e0dece75",
	"./sk.js": "e0600ff36ac1e0dece75",
	"./sl": "46cd6d95bdea594f4775",
	"./sl.js": "46cd6d95bdea594f4775",
	"./sq": "c043969c123ab5cd2b70",
	"./sq.js": "c043969c123ab5cd2b70",
	"./sr": "77fd666a52737f1efd7b",
	"./sr-cyrl": "6868054dbb36f02db227",
	"./sr-cyrl.js": "6868054dbb36f02db227",
	"./sr.js": "77fd666a52737f1efd7b",
	"./ss": "bbaffbb41ef7f4fd8c3a",
	"./ss.js": "bbaffbb41ef7f4fd8c3a",
	"./sv": "be84e9b3b463cb96fa5a",
	"./sv.js": "be84e9b3b463cb96fa5a",
	"./sw": "c35940c6626d2bccf0f9",
	"./sw.js": "c35940c6626d2bccf0f9",
	"./ta": "73e1ad1f793625d5e923",
	"./ta.js": "73e1ad1f793625d5e923",
	"./te": "96fe0dfde527a883c0a4",
	"./te.js": "96fe0dfde527a883c0a4",
	"./tet": "73089628077a1b3908a1",
	"./tet.js": "73089628077a1b3908a1",
	"./th": "4331da7acfa5f52746d6",
	"./th.js": "4331da7acfa5f52746d6",
	"./tl-ph": "b965a0000a1179485d25",
	"./tl-ph.js": "b965a0000a1179485d25",
	"./tlh": "a42a137c2d01e40c18cd",
	"./tlh.js": "a42a137c2d01e40c18cd",
	"./tr": "183a75bd2437f11817b2",
	"./tr.js": "183a75bd2437f11817b2",
	"./tzl": "42e54a873080a091cf1e",
	"./tzl.js": "42e54a873080a091cf1e",
	"./tzm": "48caaadb1d262b8d3df6",
	"./tzm-latn": "d974062d0d2321cab039",
	"./tzm-latn.js": "d974062d0d2321cab039",
	"./tzm.js": "48caaadb1d262b8d3df6",
	"./uk": "917876f36b35aa56a2cd",
	"./uk.js": "917876f36b35aa56a2cd",
	"./ur": "9c29a77b375c4c9c03a3",
	"./ur.js": "9c29a77b375c4c9c03a3",
	"./uz": "3b78af3fb5d1ef120da2",
	"./uz-latn": "54ff3dc03d2cb362f2dc",
	"./uz-latn.js": "54ff3dc03d2cb362f2dc",
	"./uz.js": "3b78af3fb5d1ef120da2",
	"./vi": "63f81a840f588ddc77ce",
	"./vi.js": "63f81a840f588ddc77ce",
	"./x-pseudo": "51f18a209d286b73de60",
	"./x-pseudo.js": "51f18a209d286b73de60",
	"./yo": "cc6e20f395448f8d5fc0",
	"./yo.js": "cc6e20f395448f8d5fc0",
	"./zh-cn": "a25e83e8799759c246e1",
	"./zh-cn.js": "a25e83e8799759c246e1",
	"./zh-hk": "1bec13affc76a58d17d9",
	"./zh-hk.js": "1bec13affc76a58d17d9",
	"./zh-tw": "dba649134b180837c44d",
	"./zh-tw.js": "dba649134b180837c44d"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "94e4968e996275b7b27a";

/***/ }),

/***/ "a0b44f697ce670c71544":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return maxNumberOfSingleBets; });
/* unused harmony export MULTI_WIN_PLACE */
/* unused harmony export MULTI_EXOTICS */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return maxPayoutAllowed; });
/* unused harmony export TIME_TO_SHOW_WARNING */
/* unused harmony export TIME_TO_SHOW_WARNING_TIMER_ICON */
/* unused harmony export priceSourceCode */
/* unused harmony export streamingDefaultCurrency */
/* unused harmony export streamingMinBetPerCurrency */
/* unused harmony export getCountryNameFromCountryCode */
/* unused harmony export getAllCountries */
/* unused harmony export parlaysDef */
/* unused harmony export getMultiBetDisplayText */
var maxNumberOfSingleBets=10;var MULTI_WIN_PLACE='multi_winplace';var MULTI_EXOTICS='multi_exotics';var maxPayoutAllowed=1000*1000;var TIME_TO_SHOW_WARNING=10*60;var TIME_TO_SHOW_WARNING_TIMER_ICON=30;var priceSourceCode='UNITAB';var streamingDefaultCurrency='GBP';var streamingMinBetPerCurrency=new Map([['GBP',1],['EUR',1],['CZK',30],['USD',1],['BGN',2],['CHF',1],['CAD',1],['SEK',10],['HUF',350],['RON',5],['NOK',10],['INR',80],['AUD',2],['NZD',2],['HRK',10],['PLN',5],['DKK',10],['BRL',5]]);var allCountryNamesAndCodes=[{countryCode:'ENG-IRE-SCO-WAL',countryName:'UK & Ireland'},{countryCode:'GBR',countryName:'UK & Ireland'},{countryCode:'IRL',countryName:'UK & Ireland'},{countryCode:'GBR-IRL',countryName:'UK & Ireland'},{countryCode:'AUS',countryName:'Australia'},{countryCode:'IND',countryName:'India'},{countryCode:'ZAF',countryName:'South Africa'},{countryCode:'FRA',countryName:'France'},{countryCode:'UAE',countryName:'Dubai (UAE)'},{countryCode:'USA',countryName:'United States'},{countryCode:'CAN',countryName:'Canada'},{countryCode:'HKG',countryName:'Hong Kong'},{countryCode:'JPN',countryName:'Japan'},{countryCode:'SGP',countryName:'Singapore'},{countryCode:'NZL',countryName:'New Zealand'},{countryCode:'SWE',countryName:'Sweden'},{countryCode:'DEU',countryName:'Germany'},{countryCode:'NLD',countryName:'Netherlands'},{countryCode:'CHI',countryName:'Chile'},{countryCode:'URY',countryName:'Uruguay'},{countryCode:'ARG',countryName:'Argentina'},{countryCode:'PER',countryName:'Peru'},{countryCode:'ITA',countryName:'Italy'},{countryCode:'TUR',countryName:'Turkey'},{countryCode:'CZE',countryName:'Czech Republic'},{countryCode:'KOR',countryName:'Korea'},{countryCode:'FIN',countryName:'Finland'},{countryCode:'NOR',countryName:'Norway'},{countryCode:'MUS',countryName:'Mauritius'}];var getCountryNameFromCountryCode=function getCountryNameFromCountryCode(countryCode){var foundCountryNames=allCountryNamesAndCodes.filter(function(countryObject){return countryObject.countryCode.toLowerCase().split('-').indexOf(countryCode.toLowerCase().split('-')[0])>-1;});return foundCountryNames.length>0?foundCountryNames[0].countryName:'';};var getAllCountries=function getAllCountries(raceType){raceType=raceType?raceType.toUpperCase():'ALL';switch(raceType){case'T':return[{countryCode:'ALL',countryName:'ALL COUNTRIES'},{countryCode:'GBR-IRL',countryName:'UK & IRE'},{countryCode:'AUS',countryName:'AUS'},{countryCode:'IND',countryName:'IND'},{countryCode:'ZAF',countryName:'ZAF'},{countryCode:'FRA',countryName:'FRA'},{countryCode:'UAE',countryName:'UAE'},{countryCode:'USA',countryName:'USA'},{countryCode:'CAN',countryName:'CAN'},{countryCode:'HKG',countryName:'HKG'},{countryCode:'JPN',countryName:'JPN'},{countryCode:'SGP',countryName:'SGP'},{countryCode:'NZL',countryName:'NZL'},{countryCode:'SWE',countryName:'SWE'},{countryCode:'NOR',countryName:'NOR'},{countryCode:'DEU',countryName:'GER'},{countryCode:'NLD',countryName:'NLD'},{countryCode:'CHI',countryName:'CHL'},{countryCode:'URY',countryName:'URY'},{countryCode:'ARG',countryName:'ARG'},{countryCode:'PER',countryName:'PER'},{countryCode:'ITA',countryName:'ITA'},{countryCode:'TUR',countryName:'TUR'},{countryCode:'CZE',countryName:'CZE'},{countryCode:'KOR',countryName:'KOR'},{countryCode:'MUS',countryName:'MUS'}];case'G':return[{countryCode:'ALL',countryName:'ALL COUNTRIES'},{countryCode:'GBR-IRL',countryName:'UK & IRE'},{countryCode:'AUS',countryName:'AUS'},{countryCode:'USA',countryName:'USA'},{countryCode:'NZL',countryName:'NZL'},{countryCode:'KOR',countryName:'KOR'},{countryCode:'ITA',countryName:'ITA'}];case'H':return[{countryCode:'ALL',countryName:'ALL COUNTRIES'},{countryCode:'AUS',countryName:'AUS'},{countryCode:'FRA',countryName:'FRA'},{countryCode:'USA',countryName:'USA'},{countryCode:'CAN',countryName:'CAN'},{countryCode:'SWE',countryName:'SWE'},{countryCode:'DEU',countryName:'GER'},{countryCode:'NLD',countryName:'NLD'},{countryCode:'FIN',countryName:'FIN'},{countryCode:'NOR',countryName:'NOR'},{countryCode:'NZL',countryName:'NZL'},{countryCode:'ARG',countryName:'ARG'},{countryCode:'ITA',countryName:'ITA'}];default:return[{countryCode:'ALL',countryName:'ALL COUNTRIES'},{countryCode:'GBR-IRL',countryName:'UK & IRE'},{countryCode:'AUS',countryName:'AUS'},{countryCode:'IND',countryName:'IND'},{countryCode:'ZAF',countryName:'ZAF'},{countryCode:'FRA',countryName:'FRA'},{countryCode:'UAE',countryName:'UAE'},{countryCode:'USA',countryName:'USA'},{countryCode:'CAN',countryName:'CAN'},{countryCode:'HKG',countryName:'HKG'},{countryCode:'JPN',countryName:'JPN'},{countryCode:'SGP',countryName:'SGP'},{countryCode:'NZL',countryName:'NZL'},{countryCode:'SWE',countryName:'SWE'},{countryCode:'NOR',countryName:'NOR'},{countryCode:'DEU',countryName:'GER'},{countryCode:'NLD',countryName:'NLD'},{countryCode:'CHI',countryName:'CHL'},{countryCode:'URY',countryName:'URY'},{countryCode:'ARG',countryName:'ARG'},{countryCode:'PER',countryName:'PER'},{countryCode:'ITA',countryName:'ITA'},{countryCode:'KOR',countryName:'KOR'}];}};var parlaysDef=[{name:'trixie',betTypes:[{name:'doubles',combinations:3},{name:'treble',combinations:1}]},{name:'patent',betTypes:[{name:'singles',combinations:3},{name:'doubles',combinations:3},{name:'treble',combinations:1}]},{name:'yankee',betTypes:[{name:'doubles',combinations:6},{name:'trebles',combinations:4},{name:'fourfold',combinations:1}]},{name:'lucky15',betTypes:[{name:'singles',combinations:4},{name:'doubles',combinations:6},{name:'trebles',combinations:4},{name:'fourfold',combinations:1}]},{name:'lucky31',betTypes:[{name:'singles',combinations:5},{name:'doubles',combinations:10},{name:'trebles',combinations:10},{name:'fourfolds',combinations:5},{name:'fivefold',combinations:1}]},{name:'canadian',betTypes:[{name:'doubles',combinations:10},{name:'trebles',combinations:10},{name:'fourfolds',combinations:5},{name:'fivefold',combinations:1}]},{name:'lucky63',betTypes:[{name:'singles',combinations:6},{name:'doubles',combinations:15},{name:'trebles',combinations:20},{name:'fourfolds',combinations:15},{name:'fivefolds',combinations:6},{name:'sixfold',combinations:1}]},{name:'heinz',betTypes:[{name:'doubles',combinations:15},{name:'trebles',combinations:20},{name:'fourfolds',combinations:15},{name:'fivefolds',combinations:6},{name:'sixfold',combinations:1}]},{name:'superHeinz',betTypes:[{name:'doubles',combinations:21},{name:'trebles',combinations:35},{name:'fourfolds',combinations:35},{name:'fivefolds',combinations:21},{name:'sixfolds',combinations:7},{name:'sevenfold',combinations:1}]},{name:'goliath',betTypes:[{name:'doubles',combinations:28},{name:'trebles',combinations:56},{name:'fourfolds',combinations:70},{name:'fivefolds',combinations:56},{name:'sixfolds',combinations:28},{name:'sevenfolds',combinations:8},{name:'eightfold',combinations:1}]}];var getMultiBetDisplayText=function getMultiBetDisplayText(multiType,numberOfBets){if(multiType){multiType=multiType.toLowerCase().replace('eachway','');switch(multiType){case'double':return numberOfBets>2?multiType+'s':multiType;case'treble':return numberOfBets>3?multiType+'s':multiType;case'fourfold':return numberOfBets>4?multiType+'s':multiType;case'fivefold':return numberOfBets>5?multiType+'s':multiType;case'sixfold':return numberOfBets>6?multiType+'s':multiType;case'sevenfold':return numberOfBets>7?multiType+'s':multiType;case'eightfold':return numberOfBets>8?multiType+'s':multiType;case'ninefold':return numberOfBets>9?multiType+'s':multiType;default:return multiType;}}return multiType;};

/***/ }),

/***/ "c666a2f24dc3a8111cb2":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.js
var es_symbol = __webpack_require__("156e15eb0ffe21ef81ad");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.description.js
var es_symbol_description = __webpack_require__("930a5d70128dea147332");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.async-iterator.js
var es_symbol_async_iterator = __webpack_require__("a7dbc43ae2798d16a579");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.has-instance.js
var es_symbol_has_instance = __webpack_require__("94a3ded213a38dd35ea4");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.is-concat-spreadable.js
var es_symbol_is_concat_spreadable = __webpack_require__("e4e0e0ab2647e9034d9c");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.iterator.js
var es_symbol_iterator = __webpack_require__("694c7f1c520dae44c684");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.match.js
var es_symbol_match = __webpack_require__("040813b637973ee20255");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.replace.js
var es_symbol_replace = __webpack_require__("bdfc9f850f0e85956754");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.search.js
var es_symbol_search = __webpack_require__("16832efedf31c7afbad4");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.species.js
var es_symbol_species = __webpack_require__("18cf5017f5242913e65e");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.split.js
var es_symbol_split = __webpack_require__("3fff3a37c866b626bf8c");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.to-primitive.js
var es_symbol_to_primitive = __webpack_require__("d6b316e4fa01bd5f7030");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.to-string-tag.js
var es_symbol_to_string_tag = __webpack_require__("0d397c707c060ae5a205");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.symbol.unscopables.js
var es_symbol_unscopables = __webpack_require__("18034d1e20d3dc6a1200");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.concat.js
var es_array_concat = __webpack_require__("f701f5ba8dd9267f7597");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.copy-within.js
var es_array_copy_within = __webpack_require__("c8d9df74fea6eaf9e5c0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.every.js
var es_array_every = __webpack_require__("f0ecb5e1acf024da4e83");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.fill.js
var es_array_fill = __webpack_require__("e59e1c756db5df5bc3b9");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.filter.js
var es_array_filter = __webpack_require__("aee243f252d382c9e099");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.find.js
var es_array_find = __webpack_require__("c8d99034bfbc976a4839");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.find-index.js
var es_array_find_index = __webpack_require__("e1e7df29006c8ae8a3c1");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.flat.js
var es_array_flat = __webpack_require__("008118396ffa1b2054e0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.flat-map.js
var es_array_flat_map = __webpack_require__("52b9bfb4b17bf739f1eb");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.for-each.js
var es_array_for_each = __webpack_require__("71af1170aba5de7dbb34");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.from.js
var es_array_from = __webpack_require__("3ef6222168140b1cf9bc");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.includes.js
var es_array_includes = __webpack_require__("9560ae0e863d03c5f316");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.index-of.js
var es_array_index_of = __webpack_require__("06ff1bec695638dc8be4");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.iterator.js
var es_array_iterator = __webpack_require__("cf5ea858c910378e96c3");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.join.js
var es_array_join = __webpack_require__("98ac8ced001d624249cd");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.last-index-of.js
var es_array_last_index_of = __webpack_require__("792a0ce59946b43e86b7");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.map.js
var es_array_map = __webpack_require__("be92b4822cb7f54ccc11");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.of.js
var es_array_of = __webpack_require__("2f6ac45909aecf42e505");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.reduce.js
var es_array_reduce = __webpack_require__("abe2da86011b633e14fe");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.reduce-right.js
var es_array_reduce_right = __webpack_require__("e2fbe14901cd175e6b17");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.reverse.js
var es_array_reverse = __webpack_require__("9fff14e8b216210fba3b");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.slice.js
var es_array_slice = __webpack_require__("c7c67f6c83a234360c8a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.some.js
var es_array_some = __webpack_require__("10bec712757275a1384a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.sort.js
var es_array_sort = __webpack_require__("6af96a0252057fa680be");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.species.js
var es_array_species = __webpack_require__("de1efaf27dc345ba5390");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.splice.js
var es_array_splice = __webpack_require__("7c38c3c508ba38fc2a83");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.unscopables.flat.js
var es_array_unscopables_flat = __webpack_require__("71143d35f2e9b8caed4a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array.unscopables.flat-map.js
var es_array_unscopables_flat_map = __webpack_require__("677cd1a7502bf17b45fa");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array-buffer.constructor.js
var es_array_buffer_constructor = __webpack_require__("1a7932666a16ab95f8c5");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array-buffer.is-view.js
var es_array_buffer_is_view = __webpack_require__("8769740b200da340e337");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.array-buffer.slice.js
var es_array_buffer_slice = __webpack_require__("a18601ec0d09687150b9");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.data-view.js
var es_data_view = __webpack_require__("30f17b83538cf3aae560");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.date.to-iso-string.js
var es_date_to_iso_string = __webpack_require__("75ab1e7adb7cc1c66b11");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.date.to-json.js
var es_date_to_json = __webpack_require__("c7bd4ceff48e6a3ae7c2");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.date.to-primitive.js
var es_date_to_primitive = __webpack_require__("f0b88f3a8c4b98391988");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.date.to-string.js
var es_date_to_string = __webpack_require__("7f9fecfd348a0ca76c3c");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.function.bind.js
var es_function_bind = __webpack_require__("c97e2da1ae7f583527cb");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.function.has-instance.js
var es_function_has_instance = __webpack_require__("5d84c7afdc9a83a22411");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.function.name.js
var es_function_name = __webpack_require__("102ed86af417c34f8d8e");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.json.to-string-tag.js
var es_json_to_string_tag = __webpack_require__("a496c4a76612dc47b504");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.map.js
var es_map = __webpack_require__("ca8bc805c8c6ac05d898");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.acosh.js
var es_math_acosh = __webpack_require__("20f175900463cae508b5");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.asinh.js
var es_math_asinh = __webpack_require__("89a59d50fd4250c8d070");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.atanh.js
var es_math_atanh = __webpack_require__("2276f92517f3495649d1");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.cbrt.js
var es_math_cbrt = __webpack_require__("4ec3a9c45cf08040d8b9");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.clz32.js
var es_math_clz32 = __webpack_require__("e3e66f539a53e80a7ebb");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.cosh.js
var es_math_cosh = __webpack_require__("2965c19f519e65c1e451");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.expm1.js
var es_math_expm1 = __webpack_require__("cec7ef77528c8cfe128f");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.fround.js
var es_math_fround = __webpack_require__("0b2312af4ae17d7d6ff6");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.hypot.js
var es_math_hypot = __webpack_require__("1c77a8d713ca527d86cd");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.imul.js
var es_math_imul = __webpack_require__("c4fcc4ed34170557b29f");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.log10.js
var es_math_log10 = __webpack_require__("40f8268a372a4c1c592c");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.log1p.js
var es_math_log1p = __webpack_require__("89c80a9ed34d0116debc");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.log2.js
var es_math_log2 = __webpack_require__("238430e146de4002f4fa");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.sign.js
var es_math_sign = __webpack_require__("01783f198011cd065aa4");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.sinh.js
var es_math_sinh = __webpack_require__("c395fd3d359e5cb542ba");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.tanh.js
var es_math_tanh = __webpack_require__("ba34b0bdea9bc89849be");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.to-string-tag.js
var es_math_to_string_tag = __webpack_require__("8fb8869a9be92dd17843");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.math.trunc.js
var es_math_trunc = __webpack_require__("3851fa5639f2c44e5c91");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.constructor.js
var es_number_constructor = __webpack_require__("0f85e13c474b377ae8a0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.epsilon.js
var es_number_epsilon = __webpack_require__("be40b8100ed3ab2f36e6");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.is-finite.js
var es_number_is_finite = __webpack_require__("27987d1e25b74ac9ca5b");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.is-integer.js
var es_number_is_integer = __webpack_require__("d68da9a0f804df050bd3");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.is-nan.js
var es_number_is_nan = __webpack_require__("41e1c1278a99cf9d7fd2");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.is-safe-integer.js
var es_number_is_safe_integer = __webpack_require__("d2a53c2ab908028d10ed");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.max-safe-integer.js
var es_number_max_safe_integer = __webpack_require__("1e4efa52acf72001d46f");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.min-safe-integer.js
var es_number_min_safe_integer = __webpack_require__("3dd86528e2800f39bdc6");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.parse-float.js
var es_number_parse_float = __webpack_require__("a330fcc2fd15de5e1715");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.parse-int.js
var es_number_parse_int = __webpack_require__("629960a7fb34cff73505");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.to-fixed.js
var es_number_to_fixed = __webpack_require__("5337d855a80853847a2a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.number.to-precision.js
var es_number_to_precision = __webpack_require__("3226b6485d4d2aba964d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.assign.js
var es_object_assign = __webpack_require__("d74500aba3b3c0a9555d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.create.js
var es_object_create = __webpack_require__("2d326da2a97e19d9d37b");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.define-getter.js
var es_object_define_getter = __webpack_require__("07c84128c8bf9ea542bb");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.define-properties.js
var es_object_define_properties = __webpack_require__("d4304e7d6bdcd443b750");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.define-property.js
var es_object_define_property = __webpack_require__("a53b5e1eb205af881498");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.define-setter.js
var es_object_define_setter = __webpack_require__("f30973b240446fed729e");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.entries.js
var es_object_entries = __webpack_require__("372d3eb900ed8421ba5a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.freeze.js
var es_object_freeze = __webpack_require__("05982fbc019562f1efe6");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.from-entries.js
var es_object_from_entries = __webpack_require__("14f9c85236bc563c9ded");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.get-own-property-descriptor.js
var es_object_get_own_property_descriptor = __webpack_require__("cd527224333f8fb65ecd");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.get-own-property-descriptors.js
var es_object_get_own_property_descriptors = __webpack_require__("703bea8fdce723c8f746");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.get-own-property-names.js
var es_object_get_own_property_names = __webpack_require__("82bef63a648454ebc6d2");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.get-prototype-of.js
var es_object_get_prototype_of = __webpack_require__("40817af609f033a842ec");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.is.js
var es_object_is = __webpack_require__("aa129d36115b593673ec");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.is-extensible.js
var es_object_is_extensible = __webpack_require__("e01f101a740020d3f253");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.is-frozen.js
var es_object_is_frozen = __webpack_require__("a982631f670987506692");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.is-sealed.js
var es_object_is_sealed = __webpack_require__("c58a3c5c1a74160e7113");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.keys.js
var es_object_keys = __webpack_require__("9bf0cff4074afe1a9974");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.lookup-getter.js
var es_object_lookup_getter = __webpack_require__("256b77f2f0540d996e2a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.lookup-setter.js
var es_object_lookup_setter = __webpack_require__("97842536e9f8d7dc7389");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.prevent-extensions.js
var es_object_prevent_extensions = __webpack_require__("234b6023aefe6ef483ab");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.seal.js
var es_object_seal = __webpack_require__("4c7482204f45438200e6");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.set-prototype-of.js
var es_object_set_prototype_of = __webpack_require__("4abdb05252dee50eb37b");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.to-string.js
var es_object_to_string = __webpack_require__("09e0c53ae5bee45c307f");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.object.values.js
var es_object_values = __webpack_require__("cac73b4f300037083022");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.parse-float.js
var es_parse_float = __webpack_require__("9a11a163e88520ed3d34");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.parse-int.js
var es_parse_int = __webpack_require__("5fd21b853e685751f699");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.promise.js
var es_promise = __webpack_require__("19c1facf9a9022ed3679");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.promise.finally.js
var es_promise_finally = __webpack_require__("00efd37d3f5135479189");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.apply.js
var es_reflect_apply = __webpack_require__("be007ba1fcf5304a758d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.construct.js
var es_reflect_construct = __webpack_require__("d4c21a36d9b70abd94e8");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.define-property.js
var es_reflect_define_property = __webpack_require__("f0346c18857f877a2960");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.delete-property.js
var es_reflect_delete_property = __webpack_require__("fab733791092f4d899f5");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.get.js
var es_reflect_get = __webpack_require__("7b8ee7ec919901351385");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.get-own-property-descriptor.js
var es_reflect_get_own_property_descriptor = __webpack_require__("c0e774ceff34b0bccf2c");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.get-prototype-of.js
var es_reflect_get_prototype_of = __webpack_require__("060af49dfe7de424c4ed");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.has.js
var es_reflect_has = __webpack_require__("9b0e6debb4bd47d75005");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.is-extensible.js
var es_reflect_is_extensible = __webpack_require__("bdba03f622b74a498f28");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.own-keys.js
var es_reflect_own_keys = __webpack_require__("5be2d92c6d43e8e4b1bc");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.prevent-extensions.js
var es_reflect_prevent_extensions = __webpack_require__("08aa9f9cee9caa2fedb8");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.set.js
var es_reflect_set = __webpack_require__("6d9a5ca1f429444a9e8e");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.reflect.set-prototype-of.js
var es_reflect_set_prototype_of = __webpack_require__("bba10d92dc056d2e9863");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.regexp.constructor.js
var es_regexp_constructor = __webpack_require__("3a1e06cfef14972499f5");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.regexp.exec.js
var es_regexp_exec = __webpack_require__("c964841b13246e7734c2");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.regexp.flags.js
var es_regexp_flags = __webpack_require__("82f39f457e3a5afee1a9");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.regexp.to-string.js
var es_regexp_to_string = __webpack_require__("fd6eb56673f4870ab113");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.set.js
var es_set = __webpack_require__("c77694b255d0b1581c4f");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.code-point-at.js
var es_string_code_point_at = __webpack_require__("25221e8e398a6ad7a896");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.ends-with.js
var es_string_ends_with = __webpack_require__("ec9060ca9c568d5f928a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.from-code-point.js
var es_string_from_code_point = __webpack_require__("2986f15e8f8f0284f892");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.includes.js
var es_string_includes = __webpack_require__("dcf7005b28c0b100d64a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.iterator.js
var es_string_iterator = __webpack_require__("32f0472b1cb485d96f4d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.match.js
var es_string_match = __webpack_require__("f66e6d08e6925d47af65");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.pad-end.js
var es_string_pad_end = __webpack_require__("6003d36054e3c983c880");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.pad-start.js
var es_string_pad_start = __webpack_require__("ac0232c00e41755d7c86");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.raw.js
var es_string_raw = __webpack_require__("fc9c95e8ccf435980c99");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.repeat.js
var es_string_repeat = __webpack_require__("aa44f8e75a6e702855bd");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.replace.js
var es_string_replace = __webpack_require__("7be8f011839969ac4ada");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.search.js
var es_string_search = __webpack_require__("c85b9de835a854570999");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.split.js
var es_string_split = __webpack_require__("2e140452fee3258f9d1a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.starts-with.js
var es_string_starts_with = __webpack_require__("5cb70b854a317b18ba1a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.trim.js
var es_string_trim = __webpack_require__("86976a5b7a61a5d80b2f");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.trim-end.js
var es_string_trim_end = __webpack_require__("5f3d39101935715d44fd");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.trim-start.js
var es_string_trim_start = __webpack_require__("84a19e777e45478a23ac");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.anchor.js
var es_string_anchor = __webpack_require__("c1485e90ef0f52713575");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.big.js
var es_string_big = __webpack_require__("87c4a0d2b67f4b264d3d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.blink.js
var es_string_blink = __webpack_require__("ac4ccfd5db98febb2f1a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.bold.js
var es_string_bold = __webpack_require__("3dfdad6be77964a11e6f");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.fixed.js
var es_string_fixed = __webpack_require__("fdcc48bf6732b4b178d8");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.fontcolor.js
var es_string_fontcolor = __webpack_require__("ce764bc1bce7d392e0eb");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.fontsize.js
var es_string_fontsize = __webpack_require__("002d2e07bd17d0bdc6ee");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.italics.js
var es_string_italics = __webpack_require__("b4cfe0ce9e163112e26d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.link.js
var es_string_link = __webpack_require__("20e1a92cd4ab899b79ba");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.small.js
var es_string_small = __webpack_require__("9a3c1d114d6ef1fdef3c");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.strike.js
var es_string_strike = __webpack_require__("b28b254776945b000e57");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.sub.js
var es_string_sub = __webpack_require__("d7e7536a59ab09a24180");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.string.sup.js
var es_string_sup = __webpack_require__("2e980903444cc8a6d2e0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.float32-array.js
var es_typed_array_float32_array = __webpack_require__("5b7de1c2fc0a419ae93a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.float64-array.js
var es_typed_array_float64_array = __webpack_require__("1e4317a17a4ed5157eb8");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.int8-array.js
var es_typed_array_int8_array = __webpack_require__("e377ad158ef9fbe3883a");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.int16-array.js
var es_typed_array_int16_array = __webpack_require__("f191635e38cbe4fddc08");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.int32-array.js
var es_typed_array_int32_array = __webpack_require__("38d848280e7e253676dc");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.uint8-array.js
var es_typed_array_uint8_array = __webpack_require__("9d50d02abf523c5869c9");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.uint8-clamped-array.js
var es_typed_array_uint8_clamped_array = __webpack_require__("88d3f5dae483b0aedb08");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.uint16-array.js
var es_typed_array_uint16_array = __webpack_require__("93e1cb822ac91d45fe83");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.uint32-array.js
var es_typed_array_uint32_array = __webpack_require__("e62795c3b751ef449cdf");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.copy-within.js
var es_typed_array_copy_within = __webpack_require__("fc155bcbe45bfadf0342");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.every.js
var es_typed_array_every = __webpack_require__("e022efdc000586104157");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.fill.js
var es_typed_array_fill = __webpack_require__("9a96e0b7db9bbaa68179");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.filter.js
var es_typed_array_filter = __webpack_require__("5b42fddd0dd512e93995");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.find.js
var es_typed_array_find = __webpack_require__("24bbd2f39294ddb785b4");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.find-index.js
var es_typed_array_find_index = __webpack_require__("c94557787c1e8af3e276");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.for-each.js
var es_typed_array_for_each = __webpack_require__("174e441d27325e15b09f");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.from.js
var es_typed_array_from = __webpack_require__("3d2a4695981841e5afe0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.includes.js
var es_typed_array_includes = __webpack_require__("1becb1c6b73e88bba658");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.index-of.js
var es_typed_array_index_of = __webpack_require__("5ffdfc79728486ab7076");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.iterator.js
var es_typed_array_iterator = __webpack_require__("008bd541fc1883b8b6c5");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.join.js
var es_typed_array_join = __webpack_require__("0bc97372cb3b2091f533");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.last-index-of.js
var es_typed_array_last_index_of = __webpack_require__("5cc395577c19fd439116");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.map.js
var es_typed_array_map = __webpack_require__("c392212cba9673ebaa5d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.of.js
var es_typed_array_of = __webpack_require__("63a23f46c4793fc9c761");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.reduce.js
var es_typed_array_reduce = __webpack_require__("fa0999b0e64abaacee62");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.reduce-right.js
var es_typed_array_reduce_right = __webpack_require__("38eaf33ce8ac8fd7c1af");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.reverse.js
var es_typed_array_reverse = __webpack_require__("64aa05e2799c69cc599d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.set.js
var es_typed_array_set = __webpack_require__("72e79917369ecaefba6f");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.slice.js
var es_typed_array_slice = __webpack_require__("4cb87b3548bf92a0b9e3");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.some.js
var es_typed_array_some = __webpack_require__("09b82807b22b777577df");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.sort.js
var es_typed_array_sort = __webpack_require__("71a5918712acce65b0b0");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.subarray.js
var es_typed_array_subarray = __webpack_require__("e032e89df7a9adb9547f");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.to-locale-string.js
var es_typed_array_to_locale_string = __webpack_require__("6fef45047fe9a6f99bd7");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.typed-array.to-string.js
var es_typed_array_to_string = __webpack_require__("0803e22a702f46478088");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.weak-map.js
var es_weak_map = __webpack_require__("8138b8c5eae137e09bd4");

// EXTERNAL MODULE: ./node_modules/core-js/modules/es.weak-set.js
var es_weak_set = __webpack_require__("576c51ec8c71501adb14");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom-collections.for-each.js
var web_dom_collections_for_each = __webpack_require__("4f517bc3ec49a4c4049b");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.dom-collections.iterator.js
var web_dom_collections_iterator = __webpack_require__("7292f53f4e736a76189b");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.immediate.js
var web_immediate = __webpack_require__("6c07e9b5d58510c21784");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.queue-microtask.js
var web_queue_microtask = __webpack_require__("d393f10617350000615d");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.url.js
var web_url = __webpack_require__("8c457243276c12aad487");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.url.to-json.js
var web_url_to_json = __webpack_require__("d8ad59bfd57235553ad4");

// EXTERNAL MODULE: ./node_modules/core-js/modules/web.url-search-params.js
var web_url_search_params = __webpack_require__("a2be93bbdaeca814aa8f");

// EXTERNAL MODULE: ./node_modules/@kindred-rc/components/App/App.js
var App_App = __webpack_require__("6eedcd41d4949beb418c");
var App_default = /*#__PURE__*/__webpack_require__.n(App_App);

// EXTERNAL MODULE: ./node_modules/lodash/lodash.js
var lodash = __webpack_require__("9c772359e08e81b5b3ba");

// EXTERNAL MODULE: ./node_modules/pubsub-js/src/pubsub.js
var pubsub = __webpack_require__("25fe1741dcc17f4e6225");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/constants.ts
var constants = __webpack_require__("f52b02859c8241a1acbd");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/redux/stores/index.ts + 9 modules
var stores = __webpack_require__("a6db43e64a686c1c3d4d");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/redux/utils/observeUtils.ts + 2 modules
var observeUtils = __webpack_require__("d239ac438eef2a35a55e");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/utils/webUtils.tsx
var webUtils = __webpack_require__("9edd9339fba65e20e6eb");

// CONCATENATED MODULE: ./src/frankel-uk/setup/setupAnalytics.ts
var setupAnalytics_startKissMetrics=function startKissMetrics(store){var _this=this;if(typeof _kmq==='undefined'){console.log('kiss metrics is not available on page');return;}var kissMetrixListener=function kissMetrixListener(event){var el=event.currentTarget;var feature=el.attr('data-km-feature');var props={};var propsString=el.attr('data-km-props');if(propsString){var propsArray=propsString.split(',');var keyVal;for(var i=0;i<propsArray.length;i++){keyVal=propsArray[i].split(':');props[keyVal[0]]=keyVal[1];}}if(feature){track(feature,props);}};function track(feature,props){switch(feature){case'next_to_go':case'most_popular':case'pending_bets':case'carousel_event_click':case'carousel_any_event_click':if(props['placement']){props['placement']=parseInt(props['placement'])+1;}break;}var _props={};lodash["forEach"](props,function(val,key){_props[feature+'_'+key]=val;});_kmq.push(['record',feature,_props]);}var trackingKeys={};if(webUtils["b" /* isDesktopClient */]){trackingKeys.deviceType='phone';}else if(webUtils["d" /* isTabletClient */]){trackingKeys.deviceType='tablet';}else{trackingKeys.deviceType='desktop';}trackingKeys.specificDeviceType=webUtils["a" /* browserName */];_kmq.push(['set',trackingKeys]);var elKmFeatures=document.querySelectorAll('[data-km-feature]');for(var _iterator=elKmFeatures,_isArray=Array.isArray(_iterator),_i=0,_iterator=_isArray?_iterator:_iterator[Symbol.iterator]();;){var _ref;if(_isArray){if(_i>=_iterator.length)break;_ref=_iterator[_i++];}else{_i=_iterator.next();if(_i.done)break;_ref=_i.value;}var el=_ref;el.addEventListener('click',function(event){return _this.kissMetrixListener(event);},false);}pubsub["subscribe"](constants["l" /* TOPIC_KISSMETRICS_SEND */],function(message,payload){track(payload.feature,payload.props);});Object(observeUtils["a" /* observeStore */])(store,function(){return stores["d" /* sessionStore */].isAuthenticated();},function(wasAuthenticated,isAuthenticated){if(typeof wasAuthenticated==='undefined'){return;}if(!wasAuthenticated&&isAuthenticated){var user=stores["d" /* sessionStore */].getUser();_kmq.push(['identify',user.userName]);_kmq.push(['alias',user.userName,user.customerId]);_kmq.push(['set',{client_category:user.clientCategory}]);}else{delete _kmq['identify'];delete _kmq['alias'];delete _kmq['set'];}});};/* harmony default export */ var setupAnalytics = (setupAnalytics_startKissMetrics);
// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/timers/TimersCreator.ts
var TimersCreator = __webpack_require__("cd48e3f6f2affee23c63");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/utils/cmsCommunicator.ts
var cmsCommunicator = __webpack_require__("c6e66f9d1087350b3148");

// CONCATENATED MODULE: ./src/frankel-uk/setup/setupHeartBeatToCMS.ts
/* harmony default export */ var setupHeartBeatToCMS = (function(){var activeFlag=false;var updateFlag=function updateFlag(){activeFlag=true;};document.body.addEventListener('click',function(){return updateFlag();},false);var checkHeatBeat=function checkHeatBeat(){if(activeFlag){Object(cmsCommunicator["e" /* sendHeartBeat */])();activeFlag=false;}else{}};TimersCreator["a" /* default */].add(checkHeatBeat,{requiresLogin:false,loggedinInterval:TimersCreator["a" /* default */].INTERVAL_MINUTES_TWO});});
// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/config.ts
var config = __webpack_require__("5015ee62a4c79dd067c3");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/redux/actions/session.ts
var session = __webpack_require__("3087867bbef7cabf01ba");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/redux/actions/ui.ts
var ui = __webpack_require__("f3cef6a9c2862bba5637");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/utils/authentication.ts
var authentication = __webpack_require__("da665e6cacc860e91ebd");

// CONCATENATED MODULE: ./src/frankel-uk/setup/setupKeepRacingServerTokenAlive.ts
/* harmony default export */ var setupKeepRacingServerTokenAlive = (function(dispatch,keepAliveURLApi){if(keepAliveURLApi===void 0){keepAliveURLApi='/api/v1/keepalive';}function keepSessionAliveXMLHttpRequest(url,withCredentials,useAuthToken){if(withCredentials===void 0){withCredentials=false;}if(useAuthToken===void 0){useAuthToken=false;}return new Promise(function(resolve,reject){var xhr=new XMLHttpRequest();xhr.open('GET',url);xhr.withCredentials=withCredentials;if(useAuthToken){Object(authentication["a" /* setAuthHeader */])(true,xhr);}xhr.onload=function(){if(xhr.status>=200&&xhr.status<300){resolve(xhr.response);}else{if(xhr.status===401){Object(authentication["b" /* userNotAuthorized */])();}reject(xhr.statusText);}};xhr.onerror=function(response){return console.error(response);};xhr.setRequestHeader('Content-type','application/json');xhr.send();});}function keepSessionAlive(){console.log('Keeping session alive');var ERROR_CODES=['401','InvalidToken','Unauthorized'];var url=config["default"].apiHost+keepAliveURLApi;var checkForErrors=function checkForErrors(response){var responseErrorCode=response;var isResponseErrorCodeExpected=ERROR_CODES.indexOf(responseErrorCode)>=0;if(isResponseErrorCodeExpected){dispatch(ui["a" /* sendToKissMetrics */]({feature:'keepalive_session_timeout',props:{errorCode:responseErrorCode}}));dispatch(session["c" /* sessionTimeout */]());}};keepSessionAliveXMLHttpRequest(url,true,true).then(function(response){checkForErrors(response);})["catch"](function(err){checkForErrors(err);}).then(function(){return Promise.resolve();});}TimersCreator["a" /* default */].add(keepSessionAlive,{name:'keepSessionAliveTimer',loggedinInterval:TimersCreator["a" /* default */].INTERVAL_MINUTES_TEN,requiresLogin:true});});
// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/redux/actions/auth.ts
var auth = __webpack_require__("fdbe2e77ceaa5306c980");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/utils/storage.ts
var storage = __webpack_require__("e0ed1571d99456c033dd");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/components/MyRacingBets/redux/myRacingBetsReducer.ts
var myRacingBetsReducer = __webpack_require__("7dc8df583ef3a96caa56");

// CONCATENATED MODULE: ./src/frankel-uk/setup/setupSettingsStore.ts
/* harmony default export */ var setupSettingsStore = (function(dispatch,setupHeartBeatToCMSCallback){if(stores["e" /* settingsStore */].getTicket()){if(setupHeartBeatToCMSCallback){setupHeartBeatToCMSCallback();}return dispatch(Object(auth["a" /* authenticateUserByTicket */])({ticket:stores["e" /* settingsStore */].getTicket(),m:stores["e" /* settingsStore */].isMobile(),channel:stores["e" /* settingsStore */].getChannel(),cmsUserId:stores["e" /* settingsStore */].getCMSUserId()}));}else{storage["b" /* sessionStorage */].set(myRacingBetsReducer["e" /* SESS_STORE_MY_RACING_BETS_FILTER_BY_TAB_KEY */],myRacingBetsReducer["b" /* DEFAULT_FILTER_BY_TAB_LABEL */]);storage["b" /* sessionStorage */].set(myRacingBetsReducer["d" /* SESS_STORE_MY_RACING_BETS_FILTER_BY_DATE_RANGE_KEY */],myRacingBetsReducer["a" /* DEFAULT_FILTER_BY_DATE_RANGE_DATA */]);storage["b" /* sessionStorage */].set(myRacingBetsReducer["f" /* SESS_STORE_MY_RACING_BETS_SORT_BY_KEY */],myRacingBetsReducer["c" /* DEFAULT_SORT_BY_DATA */]);}return Promise.resolve();});
// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/redux/actions/oddsLadder.ts
var oddsLadder = __webpack_require__("f92b11d5ec645f51783d");

// CONCATENATED MODULE: ./src/frankel-uk/setup/index.ts
var setupSettingStore=setupSettingsStore;
// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/redux/index.tsx + 15 modules
var redux = __webpack_require__("3f9364b490af9d0371b1");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/redux/actions/bet.ts
var actions_bet = __webpack_require__("cf06cd171b194ed8c0f4");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/relay/setupRelay.ts
var setupRelay = __webpack_require__("3bc8791c2473288a474a");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/setup/index.ts + 2 modules
var setup = __webpack_require__("f29938e08540b38d3ff7");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/utils/betSlip/common.ts
var common = __webpack_require__("469bc71856268f6869d4");

// EXTERNAL MODULE: ./node_modules/@kindred-rc/app/CreateApp.au.js
var CreateApp_au = __webpack_require__("600add51a57117eefedf");
var CreateApp_au_default = /*#__PURE__*/__webpack_require__.n(CreateApp_au);

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/redux/actions/navigate.ts + 2 modules
var actions_navigate = __webpack_require__("c013d720445c7e584ef5");

// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/common/redux/actions/settings.ts
var settings = __webpack_require__("a002af7cb9272ed140f5");

// CONCATENATED MODULE: ./src/frankel-uk/static/common/styled/theme.js
var theme={colors:{green:'#3aaa35',dark_green:'#2e7423',pure_white:'#fff',light_smoke:'#eee',medium_smoke:'#ddd',dark_smoke:'#ccc',granite:'#999',medium_grey:'#777',light_graphite:'#444',medium_graphite:'#333',dark_graphite:'#222',orange:'#ff7e00',// highlight colours
supernova_yellow:'#fec800',milano_red:'#b30a0a',light_red:'#d25454',old_red:'#fe0202',// status colours
mint:'#c4e5c2',lemon:'#fff1bf',watermelon:'#efcbcb',// gradients / backgrounds
// todo change below to gradients
turf_background:'#2d7415',carbon_background:'#333',silver:'#ddd',gold:'#ffdd5e',medium_green:'#147b45',// coming from Unibet branding
accepted_green:'#a4ce7e',processing_yellow:'#ffbb71',bets_error_red:'#ffcbcf'}};/* harmony default export */ var styled_theme = (theme);
// CONCATENATED MODULE: ./src/frankel-uk/boot.ts
var isBooted=false;var boot_store=null;var boot_dispatch=null;var boot_boot=function boot(onBoot){if(isBooted){throw new Error('App already booted. you cannot call boot more than once.');}isBooted=true;fetch("https://rsa-qa.unibet.com.au/api/v1/configuration/OddsLadder/default").then(function(data){return console.log(data);})["catch"](function(ex){return console.log(ex);});boot_store=redux["a" /* default */].getStore();boot_dispatch=boot_store.dispatch.bind(boot_store);Object(observeUtils["b" /* setupStoreListeners */])(boot_store);var App=CreateApp_au_default()({backwardsCompatibleTheme:styled_theme,overrideSettings:{graphql:{uri:config["default"].graphqlHost},rest:{getBetParlay:config["default"].apiHost+'/api/v1/bets/parlay',getOddsLadder:config["default"].apiHost+'/api/v1/configuration/OddsLadder/default',getMediaPlayerStreaming:config["default"].apiHost+"/api/v1/events/streaming"}},useBackwardsComaptibleActions:true,backwardsCompatibleLogic:{navigateToRace:function navigateToRace(payload){boot_navigateToRace(payload);},setRaceDateFilter:function setRaceDateFilter(payload){},setRaceTypeFilter:function setRaceTypeFilter(payload){boot_dispatch(Object(ui["b" /* setRaceTypeFilter */])(payload));},betslip:{generateUniqueBetHash:function generateUniqueBetHash(payload){return Object(common["a" /* generateUniqueBetHash */])(payload);},addBetAction:function addBetAction(bet){return Object(actions_bet["a" /* addBet */])(bet);},removeBetAction:function removeBetAction(bet){return Object(actions_bet["h" /* removeBet */])(bet);},updateBetAction:function updateBetAction(from,to){return Object(actions_bet["o" /* updateBet */])(from,to);}},navigateTo:function navigateTo(url){boot_dispatch(Object(actions_navigate["a" /* navigateToRoute */])({pathname:url}));},navigateToRaceByMarket:function navigateToRaceByMarket(_ref){var eventKey=_ref.eventKey,marketName=_ref.marketName,priceType=_ref.priceType;boot_navigateToRace({eventKey:eventKey});},changeOddsDisplayFormat:function changeOddsDisplayFormat(format){boot_dispatch(Object(settings["a" /* changeOddsDisplayFormat */])(format));},storeDispatch:boot_dispatch}});var storeKindredRc=App.getStore();redux["a" /* default */].compatibilityPassthrough.on(function(action){if(typeof action!=='function'){return storeKindredRc.dispatch(action);}});Object(setup["a" /* setupGlobalConfig */])(boot_dispatch);Object(setupRelay["a" /* default */])(config["default"].graphqlHost);setupSettingStore(boot_dispatch,setupHeartBeatToCMS).then(function(){setupKeepRacingServerTokenAlive(boot_dispatch);onBoot({App:App,store:boot_store});Object(cmsCommunicator["c" /* loadingComplete */])();})["catch"](function(e){});Object(oddsLadder["a" /* getOddsLadder */])(boot_dispatch);};var boot_navigateToRace=function _navigateToRace(payload){window.scrollTo(0,0);boot_dispatch(Object(actions_navigate["a" /* navigateToRoute */])({pathname:'/event/:eventKey',params:payload}));boot_dispatch(Object(ui["c" /* setSelectedEvent */])({positionInWidget:payload.positionInWidget,widgetName:payload.widgetName}));};
// EXTERNAL MODULE: ./node_modules/kindred-racing-client/src/frankel/components/MarketingWidget/MarketingWidget.tsx
var MarketingWidget_MarketingWidget = __webpack_require__("5a61372bafae9a9c27d7");

// EXTERNAL MODULE: ./node_modules/react/index.js
var react = __webpack_require__("8af190b70a6bc55c6f1b");

// EXTERNAL MODULE: ./node_modules/react-redux/es/index.js + 22 modules
var es = __webpack_require__("0a81c721557e72a0975d");

// CONCATENATED MODULE: ./src/frankel-uk/templates/web/MarketingWidget/index.tsx
var web_MarketingWidget_MarketingWidget=function MarketingWidget(props){var gameConfiguration=props.gameConfiguration;var hasPromotionsFromGameConfiguration=gameConfiguration&&gameConfiguration.promotions&&gameConfiguration.promotions.length>0;return hasPromotionsFromGameConfiguration?react["createElement"](MarketingWidget_MarketingWidget["a" /* default */],Object.assign({},props)):null;};var MarketingWidgetConnected=Object(es["connect"])(function(){return{gameConfiguration:stores["e" /* settingsStore */].getGameConfiguration()};})(react["memo"](web_MarketingWidget_MarketingWidget));/* harmony default export */ var web_MarketingWidget = (MarketingWidgetConnected);
// EXTERNAL MODULE: ./node_modules/react-dom/index.js
var react_dom = __webpack_require__("63f14ac74ce296f77f4d");

// EXTERNAL MODULE: ./node_modules/regenerator-runtime/runtime.js
var runtime = __webpack_require__("2c09af3fb5c4ba3698b3");

// CONCATENATED MODULE: ./src/frankel-uk/initShared.tsx
boot_boot(function(_ref){var App=_ref.App,store=_ref.store;var AppWrapper=react["createElement"](App,null,react["createElement"](es["Provider"],{store:store},react["createElement"](App_default.a,{MarketingWidget:react["createElement"](web_MarketingWidget,null)})));var elementToRenderIn="krc-container";var container=document.getElementById(elementToRenderIn);if(!container){throw"Element not found: "+elementToRenderIn;}react_dom["render"](AppWrapper,container);});
// CONCATENATED MODULE: ./src/frankel-uk/init.mobile.tsx


/***/ })

/******/ });
});
//# sourceMappingURL=app.js.map